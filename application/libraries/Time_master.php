 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Time_master {

    public function __construct(){
        $this->TAG = get_class($this);
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function set_indo_month(){
        $ar_month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

        return $ar_month;
    }
}
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Mainmodel_blog extends CI_Model{
    public $db2;

    public function __construct(){
        parent::__construct(); 
        date_default_timezone_set("Asia/Bangkok");
        $this->db2 = $this->load->database('db_blog', TRUE);
    }
    
    public function get_data_all($table){
    	$data = $this->db2->get($table);
    	return $data->result();
    }

    public function get_data_all_where($table, $where){
    	$data = $this->db2->get_where($table, $where);
    	return $data->result();
    }

    public function get_data_each($table, $where){
    	$data = $this->db2->get_where($table, $where);
    	return $data->row_array();
    }

    public function insert_data($table, $data){
    	$insert = $this->db2->insert($table, $data);
    	return $insert;
    }

    public function update_data($table, $set, $where){
    	$update = $this->db2->update($table, $set, $where);
    	return $update;
    }

    public function delete_data($table, $where){
    	$delete = $this->db2->delete($table, $where);
    	return $delete;
    }

    

}
?>
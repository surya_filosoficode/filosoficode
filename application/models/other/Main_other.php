<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_other extends CI_Model{
    
    public function get_promote_all($where){
    	$this->db->join("article_main am", "am.id_article_main = pa.id_article_pr");
    	$data = $this->db->get_where("pr_article pa", $where);
    	return $data->result();
    }

}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_main extends CI_Model{

	public function select_admin($where, $where_or){
		$this->db->select("id_admin, id_tipe_admin, email, username, status_active, nama_admin, nip_admin");

        $this->db->or_where("(username = '".$where_or["username"]."'");
        $this->db->or_where("email = '".$where_or["email"]."')");
        // $this->db->where($where);

        $data = $this->db->get_where("admin", $where)->row_array();
        return $data;
	}

    public function select_admin_all($where){
        $this->db->select("id_admin, username, email, status_active, nama");
        $data = $this->db->get_where("admin", $where)->result();
        return $data;
    }

    public function insert_page_main($id_kategori, $nama_page, $next_page, $waktu, $id_admin){
        $data = $this->db->query("select insert_page_main('".$id_kategori."','".$nama_page."','".$next_page."','".$waktu."','".$id_admin."') as id_page;");
        return $data;
    }

    public function get_menu_main($where){
        $this->db->join("home_page_kategori b", "a.id_kategori = b.id_kategori");
        $data = $this->db->get_where("home_page_main a", $where)->result();
        return $data;
    }

    public function get_menu_kategori($where){
        $this->db->select("sha2(id_kategori, '256') as id_kategori, nama_kategori");
        $data = $this->db->get_where("home_page_kategori", $where)->result();
        return $data;
    }

    

}
?>
<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Main_product extends CI_Model{
    
    public function get_all_product($where){
    	// $this->db->join("product_brand b", "pd.id_brand = b.id_brand");
    	$this->db->join("toko tk", "pd.id_toko = tk.id_toko");
    	$get_data = $this->db->get_where("product pd", $where);
    	return $get_data->result();
    }

}
?>
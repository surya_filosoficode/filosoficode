    <?php
        include 'template/header_menu.php';
    ?>

    <!-- ***** Breadcumb Area Start ***** -->
    <!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>About Us</h2> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <div class="post-header text-center">
        Narasi Singkat
    </div>

    <!-- ***** About Us Area Start ***** -->
    <section class="fancy-about-us-area section-padding-100" style="padding-top: 100px;">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-12">
                    <!-- <div class="section-heading heading-black text-left">
                        <h2>Narasi Singkat</h2>
                    </div> -->
                    <div class="section-heading heading-black text-left" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Filosofi_code adalah perusahaan di bidang software dan desain developer yang berpusat di kota Malang, Indonesia. Filosofi_code didirikan pada tanggal 1 Mei 2015. Semagat kami mendirikan perusahaan ini ialah untuk mejadikan Tehnologi dapat digunakan siapapun yang membutuhkan dengan mudah, cepat dan tepat hasilnya untuk membantu kegiatan manusia baik pribadi, kelompok, atau perusahaan. Tentu saja dengan biaya yang sesuai dengan apa yang akan didapatkan dari produk-produk kami.</p>
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dengan komitmen penuh dalam mengembangkan aplikasi kami ingin menghadirkan aplikasi yang sesuai kebutuhan anda, dan membantu hidup anda menjadi lebih mudah serta menyenangkan. Tentu dengan berdasar pada komitmen kami antara lain..</p>
                    </div>
                </div>
            </div>
            <div class="row" style="margin-top: 60px;">
                <div class="col-12 col-lg-12">
                    <div class="section-heading heading-black text-center">
                        <h2>Jasa Kami</h2>
                        <p>Kami adalah perusahaan jasa yang focuse pada pengembangan Software dan Desain</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-desktop"></i>
                        <h5 class="font_color">Website Development</h5>
                        <p class="font_color">Layanan kami pertama adalah jasa pembuatan website custom seperti website profil, pencatatan inventori, pencatatan keuangan, website booking tiket, administrator surat, dan banyak lainnya.</p>
                    </div>
                </div>
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-ruler-pencil"></i>
                        <h5 class="font_color">Website and Android Design</h5>
                        <p class="font_color">Layanan ke-2 yaitu pembuatan densain website dan android custom sesuai keiginan anda, seperti desain UI/UX webite profile, e-commerce, website admin, dan banyak lainnya.</p>
                    </div>
                </div>
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-android"></i>
                        <h5 class="font_color">Android Development</h5>
                        <p class="font_color">Layanan ke-3 yaitu pembuatan aplikasi android dengan fungsi dan kegunaan yang dapat disesuaikan dengan kebutuhan anda pribadi, kantor atau komunitas.</p>
                    </div>
                </div>

                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-blackboard"></i>
                        <h5 class="font_color">Other Design</h5>
                        <p class="font_color">Selain desain UI/UX untuk website dan software, kami juga menerima jasa pembuatan desain seperti undangan pernikahan, kartu nama, curiculum vitae bagi pencari kerja, proposal pengajuan usaha, desain powerpoint dan banyak lainnya.</p>
                    </div>
                </div>
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-video-camera"></i>
                        <h5 class="font_color">Video Profile</h5>
                        <p class="font_color">Layanan kami selanjutnya adalah juga pembuatan video profile untuk usaha, pribadi, instansi atau komunitas anda, selain itu kami juga menyediakan jasa untuk pembuatan video produk anda sehingga lebih menarik</p>
                    </div>
                </div>
                <!-- Single Service -->
                <div class="col-12 col-md-4">
                    <div class="single-service-area text-center wow fadeInUp" data-wow-delay="0.5s">
                        <i class="ti-camera"></i>
                        <h5 class="font_color">Photography</h5>
                        <p class="font_color">Layanan kami selanjutnya adalah jasa fotografi produk, pernikahan, pesta dan acara publik atau privat lainnya.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Us Area End ***** -->

    <!-- ***** Skills Area Start ***** -->
    <section class="fancy-skills-area section-padding-100">
        <!-- Side Thumb -->
        <div class="skills-side-thumb">
            <img src="<?php print_r(base_url());?>assets/template/img/bg-img/skills.png" alt="">
        </div>
        <!-- Skills Content -->
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 col-xl-5 ml-auto">
                    <div class="section-heading">
                        <h2>Beautiful Aplication And Desain</h2>
                        <p></p>
                    </div>

                    <div class="about-us-text" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Komitmen kami tentang produk adalah kami ingin membuat produk yang mempunyai fungsi yang sesuai kebutuhan anda, Kenyamanan anda saat menggunakan produk buatan kami, baik nyaman dalam menggunakan maupun nyaman untuk di pandang, serta keindahan desain dari setiap produk yang kami hasilkan. Semua itu merupakan fokus kami</p>

                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dengan demikian kami berharap anda dapat senang dengan pelayanan dan produk kami.</p>
                        <!-- <a href="#" class="btn fancy-btn fancy-dark">Read More</a> -->
                    </div>                    
                </div>
            </div>
        </div>
    </section>
    <!-- ***** Skills Area End ***** -->

    <!-- ***** About Us Area Start ***** -->
    <section class="fancy-about-us-area">
        <div class="container">
            <div class="row">
                <div class="col-12 col-lg-6">
                    <div class="section-heading">
                        <h2>You Feel Happy</h2>
                        <p></p>
                    </div>

                    <div class="about-us-text" align="justify">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sebuah kebanggan bagi kami ketika Aplikasi dan Desain yang kami buat dapat membantu mempermudah pekerjaan anda dan bermanfaat bagi perkembangan bisnis anda.</p>

                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tentu kebahagiaan dan keuntungan anda menjadi sebuah dorongan untuk kami terus berusaha untuk berkarya dan terus memberikan pelayanan terbaik untuk anda.</p>
                        <!-- <a href="#" class="btn fancy-btn fancy-dark">Read More</a> -->
                    </div>  
                </div>

                <div class="col-12 col-lg-6 col-xl-5 ml-xl-auto">
                    <div class="about-us-thumb wow fadeInUp" data-wow-delay="0.5s">
                        <img src="<?php print_r(base_url());?>assets/img/other/happy_work.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ***** About Us Area End ***** -->

   

    

    <?php include 'template/footer_menu.php';?>

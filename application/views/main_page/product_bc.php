<!-- ***** Produk Aplikasi dan Website ***** -->
                        <div class="row">
                            <div class="col-12">
                                <div class="section-heading text-center">
                                    <h2>Aplikasi dan Website</h2>
                                    <!-- <p>We Are A Creative Digital Agency. Focused on Growing Brands Online</p> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/my_app/sikecil_app.png" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Sikecil.id</a></h5>
                                        <p class="main_content_product">Sikecil.id Aplikasi android dan website admin untuk pencatatan UKM kecil dan menengah, seperti pencatatan pengeluaran, pemasukan, hutang, piutang dan beban.</p>
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="1s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/my_app/sikecil_app_admin.png" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Aplikasi Klasifikasi Opini</a></h5>
                                        <p class="main_content_product">Aplikasi Klasifikasi Opini ini merupakan aplikasi yang dirancang untuk memetakan opini user, cutomer atau komentar publik kedalam 3 kategori yaitu opini baik, buruk dan netral.</p>
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="1.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/my_app/pt_blessindo_app_admin.png" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Sistem Inventori dan Keuangan</a></h5>
                                        <p class="main_content_product">Sistem Inventori dan Keuangan ini di peruntukkan bagi perusahaan menegah kebawah, sebagai alat untuk mempermudah pengawasan inventori.</p>
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- ***** Produk Aplikasi dan Website ***** -->

                    <!-- ***** Website Desain ***** -->
                        <div class="row">
                            <div class="col-12">
                                <div class="section-heading text-center">
                                    <br><br><br><br>
                                    <h2>Website Desain</h2>
                                    <!-- <p>We Are A Creative Digital Agency. Focused on Growing Brands Online</p> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/web_desing/cargo.PNG" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Cargo Design</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/web_desing/news.PNG" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">News Design</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/web_desing/jual_beli_rmh.PNG" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">House Market Design</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/web_desing/restaurant.PNG" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Restaurant Design</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/web_desing/web_profile.PNG" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Web Personal Design</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/web_desing/web_profile1.PNG" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Web Profile Design</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- ***** Website Desain ***** -->

                    <!-- ***** Android Desain ***** -->
                        <div class="row">
                            <div class="col-12">
                                <div class="section-heading text-center">
                                    <br><br><br><br>
                                    <h2>Android Desain</h2>
                                    <!-- <p>We Are A Creative Digital Agency. Focused on Growing Brands Online</p> -->
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/android_design/order.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Tracking Order</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/android_design/market.png" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Easy Market</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/android_design/nice_transaction.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Nice Transaction</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/android_design/feshion.png" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Lovely Fashion</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/android_design/login.png" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Login Fun</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img class="img_product" src="<?php print_r(base_url());?>assets/img/android_design/nice_dash.gif" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Beautiful Dashboard</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- ***** Android Desain ***** -->

                    <!-- ***** Produk Desain dan Photograpy ***** -->
                        <div class="row" hidden="">
                            <div class="col-12">
                                <div class="section-heading text-center">
                                    <br><br><br><br>
                                    <h2>Desain dan Photograpy</h2>
                                    <!-- <p>We Are A Creative Digital Agency. Focused on Growing Brands Online</p> -->
                                </div>
                            </div>
                        </div>

                        <div class="row" hidden="">
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img src="<?php print_r(base_url());?>assets/img/desain/PhotoGrid_1564486811717.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">by @Azizah</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img src="<?php print_r(base_url());?>assets/img/desain/PhotoGrid_1564489223239.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">by @Azizah</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img src="<?php print_r(base_url());?>assets/template/img/blog-img/blog-3.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Device Friendly</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row" hidden="">
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img src="<?php print_r(base_url());?>assets/template/img/blog-img/blog-1.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">We Create Experiences</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img src="<?php print_r(base_url());?>assets/template/img/blog-img/blog-2.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Simple, Fast And Fun</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                            <!-- Single Blog -->
                            <div class="col-12 col-md-4">
                                <div class="single-blog-area wow fadeInUp" data-wow-delay="0.5s">
                                    <img src="<?php print_r(base_url());?>assets/template/img/blog-img/blog-3.jpg" alt="">
                                    <div class="blog-content">
                                        <h5><a href="static-page.html">Device Friendly</a></h5>
                                        <!-- <p>The Fancy that recognize the talent and effort of the best web designers, developers and agencies in the world.</p> -->
                                        <a href="static-page.html">Learn More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- ***** Produk Desain dan Photograpy ***** -->
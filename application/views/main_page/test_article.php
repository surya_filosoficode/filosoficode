<?php include 'template/header_menu.php'; ?>
<!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(http://localhost:8080/filosoficode/assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>SUPER CASHBACK - Filosofi_code bagi-bagi angpau Lebaran</h2> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <div class="post-header text-center">BERDIKARI.! bersama Filosofi_code (Grand Opening)</div>

    <div id="out_article" class="main-contents">
        <section class="single_blog_area section-padding-100">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 col-lg-12">
                        <div class="row no-gutters">

                            <!-- Single Post -->
                            <div class="col-12">
                                <div class="single-post">
                                    <!-- Post Content -->
                                    <div class="post-content">
                                        <p style="text-align: center; "><img src="http://localhost:8080/adm_filcod/assets/img/all_img/img_20200502094402_0.png" style="width: 763.808px; height: 429.188px;"><br></p>
                                        <br>

                                        <blockquote class="fancy-blockquote">
                                            <span class="quote playfair-font">“</span>
                                            <h5 class="mb-4">“<i>Stay At Home alias Dirumah saja atau Ndek omah ae”</i></h5>
                                        </blockquote>

                                        <p class="MsoNormal">Mungkin Quote itu yang akhir-akhir ini sering kalian dengar di berbagai
                                        media di seluruh belahan dunia. Iya benar sekali, semua memang berubah sejak
                                        negara api menyerang (virus corona mewabah maksudnya). Mengingat kecepatan
                                        infeksinya dan begitu banyak korban yang berjatuhan dan mengguncang dunia
                                        hingga menjadikan virus ini menjadi wabah internasional terparah sepanjang masa.
                                        Terlepas dari segala perdebatan yang berkaitan dengan virus ini, tentang ini
                                        sengaja dibuat oleh suatu kelompok atau bukan, yang jelas nyatanya ini virus
                                        memang ada. Faktanya korban memang semakin banyak. Kita tidak tau apa makhluk
                                        kecil itu ada di sekitar kita atau tidak.</p>

                                        <p class="MsoNormal">Hanya saja demi kebaikan umat, ada baiknya kita tetap waspda,
                                        menjaga diri, keluarga dan manusia lain disekitar kita. Oleh karena itu,
                                        tetaplah tenang dan bijak dalam menyikapi wabah ini. Kendati virus ini
                                        diinformasikan dapat disembuhkan sendiri dengan mengisolasi diri, tapi untuk
                                        sementara ini ikuti saja aturan pemerintah terkait <i>physical distancing. </i>Bukan hanya korban yang berjatuhan, namun juga
                                        ekonomi juga ikut terpuruk karena virus ini. Nah itu mengapa kita harus
                                        mengikuti aturan <i>physical distancing</i>&nbsp;ini
                                        untuk sementara waktu agar wabah corona ini cepat berlalu dan kehidupan kita
                                        dapat kembali berjalan seperti dulu. Untuk kalian, berikut kami sampaikan
                                        sedikit infromasi tentang virus corona.</p>


                                        <br>
                                        <p class="MsoNormal">
                                            <b>
                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;
                                        mso-bidi-theme-font:minor-latin">Apa sih Corona itu.?
                                                    <o:p></o:p>
                                                </span>
                                            </b>
                                        </p>
                                        <br>
                                        <p style="text-align: center; "><img src="http://localhost:8080/adm_filcod/assets/img/all_img/img_20200502094707_0.png" style="width: 763.808px; height: 429.188px;"><br></p>
                                        <br>
                                        <p class="MsoNormal">
                                            <i>Coronavirus</i> atau virus corona merupakan keluarga besar virus yang menyebabkan infeksi saluran pernapasan atas ringan hingga sedang, seperti penyakit <i>flu</i>. Banyak orang terinfeksi virus ini, setidaknya satu kali dalam hidupnya. Namun, beberapa jenis virus corona juga bisa menimbulkan penyakit yang lebih serius, seperti:
                                        </p>
                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l1 level1 lfo4;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Middle East Respiratory Syndrome (MERS-CoV).
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l1 level1 lfo4;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Severe Acute Respiratory Syndrome (SARS-CoV).
                                        </p>
                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l1 level1 lfo4;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Pneumonia.
                                        </p>
                                        <p class="MsoNormal">SARS yang muncul pada November 2002 di Tiongkok, menyebar ke
                                        beberapa negara lain. Mulai dari Hongkong, Vietnam, Singapura, Indonesia,
                                        Malaysia, Inggris, Italia, Swedia, Swiss, Rusia, hingga Amerika Serikat.
                                        Epidemi SARS yang berakhir hingga pertengahan 2003 itu menjangkiti 8.098 orang
                                        di berbagai negara. Setidaknya 774 orang mesti kehilangan nyawa akibat penyakit
                                        infeksi saluran pernapasan berat tersebut. Sampai saat ini terdapat tujuh 
                                            <i>coronavirus</i> (HCoVs) yang telah
                                        diidentifikasi, yaitu:
                                        </p>
                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->HCoV-229E.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->HCoV-OC43.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->HCoV-NL63.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->HCoV-HKU1.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->SARS-COV (yang menyebabkan sindrom pernapasan akut).
                                        </p>
                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l2 level1 lfo5;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->MERS-COV (sindrom pernapasan Timur Tengah).
                                        </p>

                                        <p class="MsoNormal">COVID-19 atau dikenal juga dengan Novel 
                                            <i>Coronavirus</i> (menyebabkan wabah pneumonia di kota Wuhan, Tiongkok
                                        pada Desember 2019, dan menyebar ke negara lainnya mulai Januari 2020.
                                        Indonesia sendiri mengumumkan adanya kasus covid 19 dari Maret 2020.
                                            
                                        </p>
                                        <p class="MsoNormal">Sumber: 
                                            <span class="MsoHyperlink">
                                                <span style="color: windowtext;">
                                                    <a href="https://www.halodoc.com/kesehatan/coronavirus">
                                                        <span style="color: windowtext;">halodoc.com/kesehatan/coronavirus</span>
                                                    </a>
                                                </span>
                                            </span>
                                        </p>

                                        <br>

                                        <p class="MsoNormal">
                                            <b>
                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Bahayanya dan cara penularan Covid19       
                                                </span>
                                            </b>
                                        </p>
                                        <p class="MsoNormal">
                                            Bahaya virus corona atau Covid-19 yaitu transmisi
                                        yang cepat dan lebih mudah dibandingkan wabah SARS yang pernah melanda dunia
                                        pada tahun 2003, dikutip dari 
                                                <i>Financial
                                        Times</i>. Penyebaran yang cepat ini membuat kasus positif corona di dunia
                                        mencapai 935.957 per Kamis (2/4/2020) atau dalam kurun waktu 5 bulan atau sejak
                                        kasus pertama ditemukan pada November 2019. Virus corona menyerang saluran
                                        pernapasan manusia. Seseorang dapat terinfeksi dari penderita Covid-19.</p>
                                        <p class="MsoNormal">
                                            Penyakit ini dapat menyebar melalui tetesan kecil
                                        (droplet) dari hidung atau mulut pada saat batuk atau bersin. Droplet tersebut
                                        kemudian jatuh pada benda di sekitarnya. Kemudian jika ada orang lain menyentuh
                                        benda yang sudah terkontaminasi dengan droplet tersebut, lalu orang itu
                                        menyentuh mata, hidung atau mulut (segitiga wajah), maka orang itu dapat
                                        terinfeksi Covid-19. Bisa juga seseorang terinfeksi Covid-19 ketika tanpa
                                        sengaja menghirup droplet dari penderita. Inilah sebabnya mengapa kita penting
                                        untuk menjaga jarak hingga kurang lebih satu meter dari orang yang sakit. Virus
                                        baru ini memiliki gejala awal seperti demam, batuk, pilek, gangguan pernapasan,
                                        sakit tenggorokan, letih, dan lesu. Dikutip dari BBC, pemeriksaan data oleh
                                        Organisasi Kesehatan Dunia (WHO) dari 56.000 pasien menunjukkan 6 persen
                                        memiliki gejala kritis seperti gangguan pada paru, septic shock hingga risiko
                                        kematian. Sebanyak 14 persen mengalami gejala berat yaitu kesulitan bernapas
                                        dan sesak napas. Sementara 80 persen lainnya memiliki gejala ringan seperti
                                        demam, batuk dan beberapa memiliki pneumonia. Meski pasien yang memiliki risiko
                                        meninggal hanya sekitar 6 persen, proporsi ini tak dapat disepelehkan. Kasus
                                        meninggal karena Covid-19 di seluruh dunia mencapai 47.245 dengan jumlah
                                        tertinggi berasal dari Italia yakni 13.155. Kasus meninggal terbanyak kedua terdapat
                                        di Spanyol dengan jumlah 9.387. Cina, sebagai negara pertama yang mendeteksi
                                        virus baru uni mencapat kasus meninggal sebanyak 3.312. Sementara di Indonesia,
                                        kasus meninggal mencapai 157, menurut data Worldometers. Lansia disebut menjadi
                                        salah satu kelompok yang sangat rentan dengan virus baru ini, termasuk mereka
                                        yang menderita penyakit lain misalnya asma, diabetes, penyakit jantung hingga
                                        tekanan darah tinggi. Kelompok ini berpotensi memiliki gejala berat hingga
                                        kritis jika terinfeksi Covid-19. Data dari Cina juga menunjukkan bahwa pria
                                        berisiko sedikit lebih tinggi meninggal akibat virus daripada wanita.</p>
                                        <p class="MsoNormal">
                                            Yang membuat virus ini lebih berbahaya karena tak
                                        semua yang terinfeksi menunjukkan gejala serius. Bahkan ada yang mengalami
                                        gejala ringan bahkan tanpa gejala atau silent carrier. Silent carrier ini sulit
                                        dideteksi sebab hanya bisa diketahui hanya melalui pemeriksaan. Sementara
                                        mereka yang tidak menunjukkan gejala, bisa saja berpikir bahwa dirinya sehat
                                        dan beraktivitas seperti biasa. Padahal ia dapat menularkan virus corona ini
                                        pada orang lain, baik di rumahnya maupun masyarakat umum lainnya, sehingga
                                        penyebaran makin meluas. Sebanyak enam dari sepuluh kasus 
                                                <i>coronavirus</i> mungkin disebabkan oleh orang yang mengalami gejala
                                        ringan atau tidak sama sekali, kata Weforum.org.</p>

                                        <p class="MsoNormal">Sumber: 
                                            <span class="MsoHyperlink">
                                                <span style="color: windowtext;">
                                                    <a href="https://tirto.id/eKdF">
                                                        <span style="color: windowtext;">tirto.id/eKdF</span>
                                                    </a>
                                                </span>
                                            </span>
                                        </p>

                                        <br>
                                        <p class="MsoNormal">
                                            <b>
                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Bagaimana gejala jikalau kalian terinfeksi Covid19
                                                </span>
                                            </b>
                                        </p>
                                        <br>
                                        <p style="text-align: center; "><img src="http://localhost:8080/adm_filcod/assets/img/all_img/img_20200502094140_0.png" style="width: 689.5px; height: 525.744px;"><br></p>
                                        <br>
                                        <p class="MsoNormal">Virus corona bisa menimbulkan beragam gejala pada pengidapnya.
                                        Gejala yang muncul ini bergantung pada jenis virus corona yang menyerang, dan
                                        seberapa serius infeksi yang terjadi. Berikut beberapa gejala virus corona yang
                                        terbilang ringan:</p>
                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Hidung beringus.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Sakit kepala.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Batuk.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Sakit tenggorokan.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Demam.
                                        </p>
                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l4 level1 lfo1;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Merasa tidak enak badan.
                                        </p>
                                        <p class="MsoNormal">Hal yang perlu ditegaskan, beberapa virus corona dapat menyebabkan
                                        gejala yang parah. Infeksinya dapat berubah menjadi bronkitis dan pneumonia
                                        (disebabkan oleh COVID-19), yang mengakibatkan gejala seperti:</p>

                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Demam yang mungkin cukup tinggi bila pasien mengidap pneumonia.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                            </span>
                                            <!--[endif]-->Batuk dengan lendir.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Sesak napas.
                                        </p>
                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l0 level1 lfo2;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Nyeri dada atau sesak saat bernapas dan batuk.
                                        </p>
                                        <p class="MsoNormal">Infeksi bisa semakin parah bila menyerang kelompok individu
                                        tertentu. Contohnya, orang dengan penyakit jantung atau paru-paru, orang dengan
                                        sistem kekebalan yang lemah, bayi, dan lansia.</p>

                                        <br>
                                        <p class="MsoNormal">
                                            <b>
                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Bagaimana cara pencegahan Covid19
                                                </span>
                                            </b>
                                        </p>

                                        <p class="MsoNormal">Sampai saat ini belum ada vaksin untuk mencegah infeksi virus
                                        corona. Namun, setidaknya ada beberapa cara yang bisa dilakukan untuk
                                        mengurangi risiko terjangkit virus ini. Berikut upaya yang bisa dilakukan:</p>
                                        <p class="MsoListParagraphCxSpFirst" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Sering-seringlah mencuci tangan dengan sabun dan air selama 20 detik hingga bersih.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Hindari menyentuh wajah, hidung, atau mulut saat tangan dalam keadaan kotor atau belum dicuci.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Hindari kontak langsung atau berdekatan dengan orang yang sakit.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Hindari menyentuh hewan atau unggas liar.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Membersihkan dan mensterilkan permukaan benda yang sering digunakan. 
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Tutup hidung dan mulut ketika bersin atau batuk dengan tisu. Kemudian, buanglah tisu dan cuci tangan hingga bersih.           
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Jangan keluar rumah dalam keadaan sakit.
                                        </p>
                                        <p class="MsoListParagraphCxSpMiddle" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Kenakan masker dan segera berobat ke fasilitas kesehatan ketika mengalami gejala penyakit saluran napas.      
                                        </p>
                                        <p class="MsoListParagraphCxSpLast" style="text-indent:-.25in;mso-list:l3 level1 lfo3;padding-right: 30px; padding-left: 70px;">
                                            <!--[if !supportLists]-->
                                            <span style="font-family:Symbol;mso-fareast-font-family:Symbol;mso-bidi-font-family:Symbol">·
                                                <span style="font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                </span>
                                            </span>
                                            <!--[endif]-->Selain itu, kamu juga bisa perkuat sistem
                                            kekebalan tubuh dengan konsumsi vitamin dan suplemen sebagai bentuk pencegahan
                                            dari virus ini.
                                        </p>

                                        <br>
                                        <p class="MsoNormal">
                                            <b>
                                                <span style="font-size:20.0pt;line-height:107%;mso-bidi-font-family:Calibri;mso-bidi-theme-font:minor-latin">Kapan harus ke dokter
                                                </span>
                                            </b>
                                        </p>
                                        <p class="MsoNormal">Segera lakukan isolasi mandiri bila Anda mengalami gejala infeksi
                                        virus Corona (COVID-19) seperti yang telah disebutkan di atas, terutama jika
                                        dalam 2 minggu terakhir Anda berada di daerah yang memiliki kasus COVID-19 atau
                                        kontak dengan penderita COVID-19. Setelah itu, hubungi hotline COVID-19 di 119
                                        Ext. 9 untuk mendapatkan pengarahan lebih lanjut.</p>
                                        <p class="MsoNormal">Bila Anda mungkin terpapar virus Corona tapi tidak mengalami
                                        gejala apa pun, Anda tidak perlu memeriksakan diri ke rumah sakit, cukup
                                        tinggal di rumah selama 14 hari dan membatasi kontak dengan orang lain. Bila
                                        muncul gejala, baru lakukan isolasi mandiri dan tanyakan kepada dokter melalui
                                        telepon atau aplikasi mengenai tindakan apa yang perlu Anda lakukan dan obat
                                        apa yang perlu Anda konsumsi.</p>
                                        <p class="MsoNormal">Sumber: 
                                            <span class="MsoHyperlink">
                                                <span style="color: windowtext;">
                                                    <a href="https://www.alodokter.com/virus-corona">
                                                        <span style="color: windowtext;">www.alodokter.com/virus-corona</span>
                                                    </a>
                                                </span>
                                            </span>
                                        </p>

                                        <p class="MsoNormal"><span class="MsoHyperlink">Sekian dulu kawan infromasi dari kami tentang corona. Ingat
                                        ya kawan tahan diri untuk sementara waktu </span>“<i>Stay
                                        At Home</i> alias Dirumah saja atau Ndek omah ae”. Jangan ngeyel, apalai kalau
                                        daerah kalian termasuk kategori merah alias banyak orang yang terinveksi.

                                        <p class="MsoNormal">Ok, Sampai jumpa di artikel filosofi_code selanjutnya. Tetap
                                        pentengin <a href="http://localhost:8080/filosoficode/page/about_us">website kami</a> atau IG kami <a href="javascript:window.open('https://www.instagram.com/filosoficode/')">@filosoficode</a> untuk dapat informasi dan promo
                                        terbaru.– Terimakasih --</p>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php include 'template/footer_menu.php';?>
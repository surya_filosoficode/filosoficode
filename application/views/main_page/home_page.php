
    <?php
        include 'template/header_menu.php';

        $str_li = "";
        $str_div = "";
        if(isset($list_pr_article)){
            if($list_pr_article){
                $no = 0;
                foreach ($list_pr_article as $key => $value) {
                    $id_article_main = hash("sha256", $value->id_article_main);
                    $link_dt_art = base_url()."page/blog?id_ses=".$id_article_main;

                    $main_img_article = $value->main_img_article;
                    $title_article = $value->title_article;

                    $content_article = str_replace('  ', '', preg_replace('/[\n]+|[\t]+/', '', strip_tags($value->content_article)));
                    $str_content_article = $content_article;
                    if(strlen($content_article) >= 250){
                        $str_content_article = substr($content_article, 0, 250);
                    }

                    $main_img_article = str_replace("base_url/", $base_url, $main_img_article);
                

                    $str_li_active = "";
                    $str_div_active = "";
                    if($no == 0){
                        $str_li_active = " class=\"active\"";
                        $str_div_active = " active";        
                    }

                    $str_li .= "<li data-target=\"#myCarousel\" data-slide-to=\"0\"".$str_li_active."></li>";

                    $str_div .= "<div class=\"carousel-item".$str_div_active."\">
                                  <img class=\"first-slide\" src=\"".$main_img_article."\" alt=\"First slide\">
                                  <div class=\"container\">
                                    <div class=\"carousel-caption text-left\">
                                      <h3 class=\"carousel-headline\">".$title_article."</h3>
                                      <p class=\"carousel-head-text\">&nbsp;&nbsp;&nbsp;&nbsp;".$str_content_article." ... 
                                        <a class=\"carousel-link-text\" href=\"".$link_dt_art."\" >Read More</a>
                                      </p>
                                      
                                    </div>
                                  </div>
                                </div>";

                    $no++;
                }
            }
        }
    ?>

    <!-- ***** Hero Area Start ***** -->
    <!-- <div class="fancy-hero-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class="fancy-hero-area bg-img bg-overlay" style="background-image: none;">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <?=$str_li?>
          </ol>
          <div class="carousel-inner">
            <?=$str_div?>
          </div>

          <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
    </div>
    
    
    <!-- ***** Hero Area End ***** -->

    <!-- ***** Top Feature Area Start ***** -->
    <div class="fancy-top-features-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="fancy-top-features-content">
                        <div class="row no-gutters">
                            <div class="col-12 col-md-4">
                                <div class="single-top-feature">
                                    <div class="single-service-area text-center" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                                        <h5><img src="<?php print_r(base_url()."assets/icon_filcod/responsive.png");?>" width="45px" height="45px"></i></h5>
                                        <h5 class="font_color">Responsive Design</h5>
                                        <!-- <p class="main_content_basic" style="justify-content: all;">Compatible dengan semua platform, nyaman digunakan, interaktif menjadikan kerja lebih flesibel.. <a class="link-single-top-feature" href="#">Read More</a></p> -->

                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="single-top-feature">
                                    <div class="single-service-area text-center" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                                        <h5><img src="<?php print_r(base_url()."assets/icon_filcod/car.png");?>" width="45px" height="45px"></i></h5>
                                        <h5 class="font_color">Fast Performance</h5>
                                        <!-- <p class="main_content_basic">Performa terbaik, akurat, dengan kecepatan maksimal membuat kerja menjadi lebih produktif tanpa mengurangi waktu santai.. <a class="link-single-top-feature" href="#">Read More</a></p> -->

                                        <!-- <p class="font_color">Layanan kami pertama adalah jasa pembuatan website custom seperti website profil, pencatatan inventori, pencatatan keuangan, website booking tiket, administrator surat, dan banyak lainnya.</p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="single-top-feature">
                                    <div class="single-service-area text-center" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInUp;">
                                        <h5><img src="<?php print_r(base_url()."assets/icon_filcod/web-design.png");?>" width="45px" height="45px"></i></h5>
                                        <h5 class="font_color">Beautifull Application</h5>
                                        <!-- <p class="main_content_basic">Aplikasi dengan performa stabil, cepat, flesibel, ditambah dengan estetika yang menarik. Menjadikan kerja terasa menyenangkan.. <a class="link-single-top-feature" href="#">Read More</a></p> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="single-top-feature" style="padding-left:80px;padding-right:80px;">
                                    <hr>
                                </div>
                            </div>
                            <div class="col-12 col-md-12">
                                <div class="single-top-feature" style="padding-left:80px;padding-right:80px;text-align: center;">
                                    <h6 class="mb-4">“<i>Compatible</i> dengan semua <i>platform</i>, nyaman digunakan, interaktif menjadikan aplikasi lebih flesibel. Dipadukan dengan performa stabil dan cepat membuat kerja menjadi ringkas, tanpa mengurangi waktu santai anda. Ditambah sentuhan <i>estetika</i> yang menarik, membuat kerja-mu menjadi lebih mudah dan menyenangkan... <i><a class="link-single-top-feature" href="<?=base_url()."page/about_us";?>">Read More</a></i>”</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Top Feature Area End ***** -->

    <!-- ***** Blog Area Start ***** -->
    <section class="fancy-blog-area section-padding-100-70">

        <?php
            if(isset($list_pr_product)){
                if($list_pr_product){
                    foreach ($list_pr_product as $key => $value) {
        
                        $id_jenis        = $value["category"]->id_jenis;
                        $align_header_pr = $value["category"]->align_header_pr;
                        $r_num_pr        = $value["category"]->r_num_pr;

                        $tgl_add_pr = $value["category"]->tgl_add_pr;
                        $add_by_pr  = $value["category"]->add_by_pr;

                        $sts_pr     = $value["category"]->sts_pr;
                        $active_pr  = $value["category"]->active_pr;

                        $nama_jenis = $value["category"]->nama_jenis;
                        $parent_id  = $value["category"]->parent_id;

        ?>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="category-home <?=$align_header_pr?>">
                        <h2>
                            <span><?=$nama_jenis?></span>
                        </h2>
                    </div>
                </div>

                <div class="col-md-12" style="margin-top: 20px;">
                    <div class="row">
        <?php
                        $product_list = $value["product"];
                        foreach ($product_list as $keyp => $valuep) {
                            $id_product     = $valuep->id_product;
                            $nama_product   = $valuep->nama_product;
                            $id_toko        = $valuep->id_toko;
                            $id_brand       = $valuep->id_brand;
                            $category_produk = $valuep->category_produk;
                            $desc_product   = $valuep->desc_product;
                            $spec_product   = $valuep->spec_product;
                            $img_list_product = $valuep->img_list_product;
                            $tag_product    = $valuep->tag_product;
                            $price_product  = $valuep->price_product;
                            $disc_product   = $valuep->disc_product;
                            $sts_pr_product = $valuep->sts_pr_product;
                            $sts_nego_product = $valuep->sts_nego_product;

                            $tipe_owner = $valuep->tipe_owner;
                            $id_owner = $valuep->id_owner;
                            $nama_toko = $valuep->nama_toko;
                            $desc_toko = $valuep->desc_toko;
                            $main_img_toko = $valuep->main_img_toko;

                            $img_list_product = str_replace("base_url/", $base_url, $img_list_product);
                            $desc_product = str_replace("base_url/", $base_url, $desc_product);
                            $main_img_toko = str_replace("base_url/", $base_url, $main_img_toko);

                            $page_img = "";
                            $one_page = json_decode($img_list_product);
                            if($one_page){
                                $page_img = $one_page[0];
                            }

                            // sts_negosiate
                                $str_sts_nego = "<span class=\"label label-orange\">Nego</span>&nbsp;&nbsp;";
                                if($sts_nego_product == "0"){
                                    $str_sts_nego = "";
                                }

                            // price
                                $t_pr_prd = $price_product - $disc_product;

                                $procentage = 0;
                                if($disc_product != 0 && $price_product != 0){
                                    $procentage = (float)$disc_product/(float)$price_product*100;
                                }

                            // sts_negosiate
                                $str_pr_product = "<p class=\"main_content_product\">".$str_sts_nego."<a href=\"".base_url()."page/contact\">Harga Nego</a></p>";
                                if($sts_pr_product == "0"){
                                    $str_pr_product = "<p class=\"main_content_product\">".$str_sts_nego."Rp. ".number_format($price_product, 0, ",", ".")."</p>";

                                    if($disc_product != 0){
                                        $str_pr_product = "<p class=\"main_content_product\">".$str_sts_nego."Rp. ".number_format($t_pr_prd, 0, ",", ".")."</p>";
                                    }
                                }

        ?>
                        <div class="col-12 col-md-4">
                            <div class="single-blog-area wow fadeInUp">
                                <img src="<?=$page_img?>" alt="">
                                <div class="blog-content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h5><a href="<?=base_url()."page/product_detail?prd=".hash("sha256", $id_product);?>"><?=$nama_product?></a></h5>
                                        </div>
                                        <div class="col-md-8">
                                            <!-- <p class="main_content_product"><span class="label label-orange">Nego</span>&nbsp;&nbsp;Rp. <?=number_format($price_product, 0, ',' , '.')?> </p> -->

                                            <?=$str_pr_product?>
                                        </div>
                                        <div class="col-md-4 text-right">
                                            <a href="<?=base_url()."page/product_detail?prd=".hash("sha256", $id_product);?>"><h4><i class="ti-layout-grid2-alt"></i></h4></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
        <?php
                        }

        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php
                    }
                }
            }
        ?>
        

        
    </section>
    <!-- ***** Blog Area End ***** -->


    <?php include 'template/footer_menu.php';?>

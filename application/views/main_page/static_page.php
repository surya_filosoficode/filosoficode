    <?php include 'template/header_menu.php';

        $main_img_article = "";
        $title_article = "";
        $content_article = "";

        if(isset($list_article)){
            if($list_article){
                $main_img_article = $list_article["main_img_article"];
                $title_article = $list_article["title_article"];
                $content_article = $list_article["content_article"];

                $content_article = str_replace("base_url/", $base_url, $content_article);
            }
        }

    ?>

    <!-- ***** Breadcumb Area Start ***** -->
    <!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>SUPER CASHBACK - Filosofi_code bagi-bagi angpau Lebaran</h2> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <div class="post-header text-center">
        <?=$title_article;?>
    </div>

    <div id="out_article" class="main-contents">
        <?=$content_article;?>
    </div>


    <?php include 'template/footer_menu.php';?>
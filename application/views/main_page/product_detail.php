    <?php include 'template/header_menu.php';

        $id_product = "";
        $nama_product = "";
        $id_toko = "";
        $id_brand = "";
        $category_produk = "";
        $desc_product = "";
        $spec_product = "";
        $img_list_product = "";
        $tag_product = "";
        $price_product = "";
        $disc_product = "";
        $sts_pr_product = "";
        $sts_nego_product = "";

        $tipe_owner = "";
        $id_owner = "";
        $nama_toko = "";
        $desc_toko = "";
        $main_img_toko = "";

        $str_li = "";
        $str_div = "";
       
        if(isset($product_detail)){
            if($product_detail){
                $id_product     = $product_detail["id_product"];
                $nama_product   = $product_detail["nama_product"];
                $id_toko        = $product_detail["id_toko"];
                $id_brand       = $product_detail["id_brand"];
                $category_produk = $product_detail["category_produk"];
                $desc_product   = $product_detail["desc_product"];
                $spec_product   = $product_detail["spec_product"];
                $img_list_product = $product_detail["img_list_product"];
                $tag_product    = $product_detail["tag_product"];
                $price_product  = $product_detail["price_product"];
                $disc_product   = $product_detail["disc_product"];
                $sts_pr_product = $product_detail["sts_pr_product"];
                $sts_nego_product = $product_detail["sts_nego_product"];

                $tipe_owner = $product_detail["tipe_owner"];
                $id_owner = $product_detail["id_owner"];
                $nama_toko = $product_detail["nama_toko"];
                $desc_toko = $product_detail["desc_toko"];
                $main_img_toko = $product_detail["main_img_toko"];

                $img_list_product = str_replace("base_url/", $base_url, $img_list_product);
                $desc_product = str_replace("base_url/", $base_url, $desc_product);
                $main_img_toko = str_replace("base_url/", $base_url, $main_img_toko);

            // img_product
                $data_image = json_decode($img_list_product);

                $no = 0;
                foreach ($data_image as $key => $value) {
                    $str_li_active = "";
                    $str_div_active = "";
                    if($no == 0){
                        $str_li_active = " class=\"active\"";
                        $str_div_active = " active";        
                    }

                    $str_li     .= "<li data-target=\"#myCarousel\" data-slide-to=\"0\"".$str_li_active."></li>";
                    $str_div    .= "<div class=\"carousel-item $str_div_active\" style=\"height: 456px;\">
                                        <img class=\"first-slide\" src=\"$value\" alt=\"First slide\" style=\"height: auto;\" onclick=\"img_click('$value')\">
                                    </div>";

                    $no++;
                }

            // sts_negosiate
                $str_sts_nego = "Bisa Negosiasi";
                if($sts_nego_product == "0"){
                    $str_sts_nego = "Tidak Bisa dinego";
                }

            // price
                $t_pr_prd = $price_product - $disc_product;

                $procentage = 0;
                if($disc_product != 0 && $price_product != 0){
                    $procentage = (float)$disc_product/(float)$price_product*100;
                }

            // sts_negosiate
                $str_pr_product = "<h6 class=\"font_color\" style=\"color: red; margin-bottom: 0px;\"><a href=\"".base_url()."page/contact\">Silahkan Hubungi Marketing Untuk Negosiasi Harga Produk</a></h6>";
                if($sts_pr_product == "0"){
                    $str_pr_product = "<h5 class=\"font_color\" margin-bottom: 0px;\">Rp. ".number_format($price_product, 0, ",", ".")."</h5>";

                    if($disc_product != 0){

                        $str_pr_product = " <h6 class=\"font_color\" style=\"color: red; margin-bottom: 0px;\"><s>Rp. ".number_format($price_product, 0, ",", ".")."</s></h6>
                                            <h5 class=\"font_color\" style=\"margin-top: 0px;\"><span class=\"label label-info\">Save: ".number_format($procentage, 0, ",", ".")."% </span>&nbsp; Rp. ".number_format($t_pr_prd, 0, ",", ".")."</h5>";
                    }
                }


                
            }
        }
    ?>

    <!-- ***** Breadcumb Area Start ***** -->
    <!-- <div class="fancy-breadcumb-area bg-img bg-overlay" style="background-image: url(<?php print_r(base_url());?>assets/template/img/bg-img/hero-1.jpg);"> -->
    <div class=".fancy-breadcumb-area bg-img-blog bg-overlay">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcumb-content text-center">
                        <!-- <h2>SUPER CASHBACK - Filosofi_code bagi-bagi angpau Lebaran</h2> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ***** Breadcumb Area End ***** -->

    <div id="out_article" class="main-contents" style="margin-top: 50px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-title">
                        <?=$nama_product?>
                    </div>
                </div>
                <div class="col-md-7">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel" style="height: auto;">
                        <ol class="carousel-indicators">
                            <?=$str_li?>
                        </ol>
                        <div class="carousel-inner" style="height: auto;">
                            <?=$str_div?>
                        </div>
                        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
                <div class="col-md-5 text-left">
                    <div class="card">
                        <div class="card-header">
                            <h6 style="font-weight: bold;font-size: 17px;color: #232d37;">Informasi Produk</h6>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label product-title-info">Toko</label>
                                        <p id="_msg_ch_repass" style="color: #474747; margin-left: 20px;font-size: 13px;"><?=$nama_toko?></p>
                                    </div>
                                </div>
                                <!-- <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label product-title-info">Jenis Produk</label>
                                        <p id="_msg_ch_repass" style="color: #474747; margin-left: 20px;font-size: 13px;"><?=$nama_product?></p>
                                    </div>
                                </div> -->
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label product-title-info">Brand</label>
                                        <p id="_msg_ch_repass" style="color: #474747; margin-left: 20px;font-size: 13px;"><?=$id_brand?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label product-title-info">Categori Produk</label>
                                        <p id="_msg_ch_repass" style="color: #474747; margin-left: 20px;font-size: 13px;min-height: 55px;"><?=implode(", ", json_decode($category_produk))?></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label product-title-info">Tag Produk</label>
                                        <p id="_msg_ch_repass" style="color: #474747; margin-left: 20px;font-size: 13px;min-height: 55px;"><?=implode(", ", json_decode($tag_product))?></p>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12" style="margin-bottom: 30px;">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="single-top-feature">
                                <div class="single-service-area text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                    <h5><i class="ti-money"></i></h5>
                                    <!-- <h6 class="font_color" style="color: red; margin-bottom: 0px;"><s>Rp. 200.000.000</s></h6>
                                    <h5 class="font_color" style="margin-top: 0px;"><span class="label label-info">Save <?=$procentage?></span>&nbsp; Rp. 200.000.000</h5> --><?=$str_pr_product?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="single-top-feature">
                                <div class="single-service-area text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                    <h5><i class="ti-gift"></i></h5>
                                    <h5 class="font_color"><?=$str_sts_nego?></h5>
                                    <h5 class="font_color">&nbsp;</h5>
                                    <!-- <div class="post-content" style="justify-content: all;">
                                        <p>Compatible dengan semua platform, nyaman digunakan, interaktif menjadikan kerja lebih flesibel.. <a class="link-single-top-feature" href="#">Read More</a></p>    
                                    </div> -->
                                </div>
                            </div>
                        </div> 
                        <div class="col-md-4">
                            <div class="single-top-feature">
                                <div class="single-service-area text-center wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                                    <h5><i class="ti-world"></i></h5>
                                    <h5 class="font_color">100% Original Product</h5>
                                    <h5 class="font_color">&nbsp;</h5>
                                    <!-- <div class="post-content" style="justify-content: all;">
                                        <p>Compatible dengan semua platform, nyaman digunakan, interaktif menjadikan kerja lebih flesibel.. <a class="link-single-top-feature" href="#">Read More</a></p>    
                                    </div> -->
                                </div>
                            </div>
                        </div> 
                    </div>
                </div>

                <!-- <div class="col-md-12">
                    <h4>Spesifikasi Produk</h4>

                    <div id="out_article" class="main-contents" style="min-height:0px;">
                        <section class="single_blog_area section-padding-100" style="padding-top: 15px;padding-bottom: 20px;">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-lg-12">
                                        <div class="row no-gutters">
                                            <div class="col-12">
                                                <div class="single-post">
                                                    <div class="post-content">
                                                        <p>Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea. Liusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui s nostrud exercitation ullamLorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                        <p>Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea. Liusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui s nostrud exercitation ullamLorem ipsum dolor sit amet, consectetur adipisicing elit. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                                        <h4>The best Wordpress Theme 2017</h4>
                                                        <p>Tiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea. Liusmod tempor incididunt ut labore et dolore magna aliqua. Ut</p>
                                                        <blockquote class="fancy-blockquote">
                                                            <span class="quote playfair-font">“</span>
                                                            <h5 class="mb-4">“If you’re going to try, go all the way. There is no other feeling like that. You will be alone with the gods, and the nights will flame with fire. You will ride life straight to perfect laughter. It’s the only good fight there is.”</h5>
                                                            <h6>Aigars Silkalns - <span>CEO DeerCreative</span></h6>
                                                        </blockquote>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>    
                    </div>
                </div> -->

                <div class="col-md-12">
                    <h4>Deskripsi Produk</h4>

                    <div id="out_article" class="main-contents">
                        <section class="single_blog_area section-padding-100" style="padding-top: 15px;padding-bottom: 20px;">
                            <div class="container">
                                <div class="row justify-content-center">
                                    <div class="col-12 col-lg-12">
                                        <div class="row no-gutters">

                                            <!-- Single Post -->
                                            <div class="col-12">
                                                <div class="single-post">
                                                    <!-- Post Content -->
                                                    <div class="post-content">
                                                        <?=$desc_product?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>    
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php include 'template/footer_menu.php';?>